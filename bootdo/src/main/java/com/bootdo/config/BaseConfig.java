package com.bootdo.config;



import com.bootdo.common.utils.RedisLink;
import com.bootdo.wx.WechatMpInRedisConfigStorage;
import com.github.binarywang.wxpay.config.WxPayConfig;
import com.github.binarywang.wxpay.service.WxPayService;
import com.github.binarywang.wxpay.service.impl.WxPayServiceImpl;
import me.chanjar.weixin.mp.api.WxMpConfigStorage;
import me.chanjar.weixin.mp.api.WxMpInMemoryConfigStorage;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * @author : zscat
 * @version : 1.0
 * @created on  : 2017/6/28  下午6:00
 */
@Configuration
public class BaseConfig {
    @Resource
    RedisConfigProperties redisConfigProperties;

    @Resource
    WxConfig wxConfig;
    /**
     * redis库
     *
     * @return
     */
    @Bean
    public RedisLink redisLink() {
        RedisLink redisLink = new RedisLink(redisConfigProperties.getHost(), redisConfigProperties.getPort(),
                redisConfigProperties.getPasswd());
        return redisLink;
    }


    @Bean
    public WxMpConfigStorage wxMpConfigStorage() {
        WxConfigProperties wxConfigProperties = wxConfig.getIfapp();

        WechatMpInRedisConfigStorage configStorage = new WechatMpInRedisConfigStorage();
        configStorage.setRedisLink(redisLink());
        configStorage.setAppId(wxConfigProperties.getAppid());
        configStorage.setSecret(wxConfigProperties.getAppsecret());
        configStorage.setToken(wxConfigProperties.getToken());
        configStorage.setAesKey(wxConfigProperties.getAeskey());
        long expiresTime = 60 * 50 * 2;
        configStorage.setExpiresTime(expiresTime);
        return configStorage;
    }

    @Bean
    public WxMpService wxMpService() {
        WxMpService wxMpService = new WxMpServiceImpl();
        wxMpService.setWxMpConfigStorage(wxMpConfigStorage());
        return wxMpService;
    }

    @Bean
    public WxPayService wxPayService() {
        WxConfigProperties wxConfigProperties = wxConfig.getIfapp();
        WxPayService wxPayService = new WxPayServiceImpl();
        WxPayConfig config = new WxPayConfig();
        config.setAppId(wxConfigProperties.getAppid());
        config.setMchId(wxConfigProperties.getPartenerId());
        config.setMchKey(wxConfigProperties.getPartenerKey());
        config.setNotifyUrl(wxConfigProperties.getSponsorPayNotify());
        config.setTradeType("JSAPI");
        wxPayService.setConfig(config);
        return wxPayService;
    }
}
