package com.bootdo.config;


import me.chanjar.weixin.mp.api.WxMpConfigStorage;
import me.chanjar.weixin.mp.api.WxMpInRedisConfigStorage;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import javax.annotation.Resource;

/**
 * @author : Hui.Wang [huzi.wh@gmail.com]
 * @version : 1.0
 * @created on  : 2017/11/7  下午8:16
 */

@Configuration
@ConfigurationProperties(prefix = "wx")
public class WxConfig {

    @Resource
    RedisConfigProperties redisConfigProperties;

    private JedisPool pool;

    private JedisPoolConfig config = new JedisPoolConfig();

    private static WxMpInRedisConfigStorage configStorage;

    private static final Logger log = LoggerFactory.getLogger(WxConfig.class);

    private WxConfigProperties ifapp;



    public WxConfigProperties getIfapp() {
        return ifapp;
    }

    public WxConfig setIfapp(WxConfigProperties ifapp) {
        this.ifapp = ifapp;
        return this;
    }




    public WxMpConfigStorage getConfig(String appKey) {
        log.info("get {} config", appKey);
        if (null == configStorage){
            this.config.setMaxTotal(10);
            this.config.setMaxIdle(80);
            this.config.setMaxWaitMillis(6000L);
            this.config.setTestWhileIdle(true);
            if (StringUtils.isBlank(redisConfigProperties.getPasswd())) {
                this.pool = new JedisPool(this.config, redisConfigProperties.getHost(), redisConfigProperties.getPort(), 6000);
            } else {
                this.pool = new JedisPool(this.config, redisConfigProperties.getHost(), redisConfigProperties.getPort() ,6000, redisConfigProperties.getPasswd());
            }
            configStorage = new WxMpInRedisConfigStorage(this.pool);
        }
            configStorage.setAppId(ifapp.getAppid());
            configStorage.setSecret(ifapp.getAppsecret());
            configStorage.setToken(ifapp.getToken());
            configStorage.setAesKey(ifapp.getAeskey());

        long expiresTime = 60 * 50 * 2;
        configStorage.setExpiresTime(expiresTime);
        log.info("{} config is: {}", appKey, configStorage);
        return configStorage;
    }
}
