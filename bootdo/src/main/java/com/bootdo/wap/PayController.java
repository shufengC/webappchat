package com.bootdo.wap;

import com.alibaba.fastjson.JSONObject;
import com.bootdo.shop.domain.TOrderDO;
import com.bootdo.shop.service.TOrderService;
import com.bootdo.utils.DateUtils;
import com.bootdo.wx.BaseController;
import com.bootdo.wx.Constant;
import com.github.binarywang.wxpay.bean.notify.WxPayNotifyResponse;
import com.github.binarywang.wxpay.bean.notify.WxPayOrderNotifyResult;
import com.github.binarywang.wxpay.bean.order.WxPayMpOrderResult;
import com.github.binarywang.wxpay.bean.request.WxPayUnifiedOrderRequest;
import com.github.binarywang.wxpay.exception.WxPayException;
import com.github.binarywang.wxpay.service.WxPayService;

import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.apache.commons.io.IOUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author : Hui.Wang [huzi.wh@gmail.com]
 * @version : 1.0
 * @created on  : 18/12/2017 4:56 PM
 */
@Controller
@RequestMapping(Constant.WX_PAY_URI)
public class PayController extends BaseController {


    @Resource
    private WxMpService wxMpService;
    @Autowired
    private TOrderService orderService;
    @Resource
    private WxPayService wxPayService;


    /**
     * 创建群空间赞助订单
     *
     * @param request
     * @param response
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"taobao/createOrder"}, method = RequestMethod.POST)
    public JSONObject createOrder(HttpServletRequest request,
                                  HttpServletResponse response,
                                  @RequestParam String wxOpenId,
                                  @RequestParam long orderid,
                                  @RequestParam BigDecimal totalprice) throws Exception {
        JSONObject result = new JSONObject();
        result.put("code", 0);
        try {

            TOrderDO order =new TOrderDO();
            order.setId(orderid);order.setTotalprice(totalprice);
            //生成微信支付签名
            result.putAll(generateWxPaySign(order, wxOpenId));
            return result;
        } catch (Exception e) {
            logger.error("sponsor/createOrder error: ", e);
            result.put("code", 110);
            result.put("msg", "出错了");
            return result;
        }

    }


    /**
     * 群空间赞助支付回调
     *
     * @param request
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = {"taobao/notify"}, method = RequestMethod.POST)
    public String payNotify(HttpServletRequest request) throws Exception {
        logger.info("wxpayNotify:------------------------------- start");
        try {

            TOrderDO sponsor = new TOrderDO();
            String xmlResult = IOUtils.toString(request.getInputStream(), request.getCharacterEncoding());
            WxPayOrderNotifyResult result = wxPayService.parseOrderNotifyResult(xmlResult);
            logger.info("result:{}", result);
            if ("SUCCESS".equals(result.getReturnCode())) {
                // 结果正确
                String openid = result.getOpenid();
                String outTradeNo = result.getOutTradeNo();
                WxMpUser user = wxMpService.getUserService().userInfo(openid);
                sponsor.setId(Long.parseLong(outTradeNo));

                sponsor.setStatus(1);
                orderService.update(sponsor);
//                sponsor.setWxHeadurl(user.getHeadImgUrl());
//                sponsor.setWxNickname(user.getNickname());
//                sponsor.setWxTransactionId(result.getTransactionId());
//                sponsor.setWxOpenId(user.getUnionId());
//
//                JSONObject data = sponsorH5Service.finishSponsor(sponsor, true);
//
//                if (null == data) {
//                    return WxPayNotifyResponse.fail("处理失败");
//                }
            }
            return WxPayNotifyResponse.success("处理成功!");
        } catch (Exception e) {
            logger.error("微信支付回调结果异常,异常原因", e);
            logger.info("wxpayNotify:------------------------------- end");
            return WxPayNotifyResponse.fail("处理失败");
        }


    }


    /**
     * 生成微信支付签名
     * - 生成赞助记录
     * - 先调用微信统一下单接口
     * - 签名并返回

     * @param wxOpenId 当前微信用户open_id
     * @return
     * @throws WxErrorException
     * @throws WxPayException
     */
    public JSONObject generateWxPaySign(TOrderDO order, String wxOpenId) throws WxErrorException, WxPayException {
        int totalFee = order.getTotalprice().intValue();

        logger.info("create sponsor order result:{}", order);
        String outTradeNo = order.getId() + "";
        WxPayUnifiedOrderRequest wxPayUnifiedOrderRequest = new WxPayUnifiedOrderRequest();
        wxPayUnifiedOrderRequest.setOutTradeNo(outTradeNo);
        wxPayUnifiedOrderRequest.setTotalFee(totalFee);
        wxPayUnifiedOrderRequest.setBody("iF赞助");
        wxPayUnifiedOrderRequest.setSpbillCreateIp("127.0.0.1");
        wxPayUnifiedOrderRequest.setOpenid(wxOpenId);
        wxPayUnifiedOrderRequest.setProductId(order.getId()+"");
        //预支付订单10分钟内有效
        Date startTime = new Date();
        Date endTime = DateUtils.addMinutes(startTime, 10);
        wxPayUnifiedOrderRequest.setTimeStart(DateUtils.getDateTime(startTime, "yyyyMMddHHmmss"));
        wxPayUnifiedOrderRequest.setTimeExpire(DateUtils.getDateTime(endTime, "yyyyMMddHHmmss"));
        wxPayUnifiedOrderRequest.setSignType("MD5");
        wxPayUnifiedOrderRequest.setTradeType("JSAPI");
        //wxPayUnifiedOrderRequest.setTradeType("MWEB"); //h5

        logger.info("generateWxPaySign:{}", wxPayUnifiedOrderRequest);

        WxPayMpOrderResult wxPayMpOrderResult = wxPayService.createOrder(wxPayUnifiedOrderRequest);

        logger.info("WxPayUnifiedOrderResult:{}", wxPayMpOrderResult);
        JSONObject result = new JSONObject();

        result.put("appId", wxPayUnifiedOrderRequest.getAppid());
        result.put("timestamp", wxPayMpOrderResult.getTimeStamp());
        result.put("nonceStr", wxPayMpOrderResult.getNonceStr());
        result.put("package", wxPayMpOrderResult.getPackageValue());
        result.put("signType", wxPayMpOrderResult.getSignType());
        result.put("paySign", wxPayMpOrderResult.getPaySign());
//        result.put("codeUrl", wxPayMpOrderResult.getMwebUrl());
        result.put("sponsorId", order.getId());
        logger.info("sign result:{}", result);
        return result;
    }
}
